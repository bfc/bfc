
/*
 * PROGRAM:
 * 	BFC
 *
 * DESCRIPTION:
 * 	BFC - BrainFuck Compiler compiles a Brainfuck code to an executable file.
 * 
 * AUTHOR:
 * 	Theophile "Tobast" BASTIAN <contact@tobast.fr>
 *
 * LICENCE:
 *  BFC - See above for description.
 *	Copyright (C) 2012 Theophile BASTIAN
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef DEF_PROCESSING
#define DEF_PROCESSING

#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>
#include "data.h"

void checkErrors(const std::vector<Instruction>& instructions);
std::string writeCCode(const std::vector<Instruction>& instructions);
std::string intToStr(int input);
std::string parseCompileLine(const std::string& cCode, const std::string& output);

#endif//DEF_PROCESSING

