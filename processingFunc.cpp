
/*
 * PROGRAM:
 * 	BFC
 *
 * DESCRIPTION:
 * 	BFC - BrainFuck Compiler compiles a Brainfuck code to an executable file.
 * 
 * AUTHOR:
 * 	Theophile "Tobast" BASTIAN <contact@tobast.fr>
 *
 * LICENCE:
 *  BFC - See above for description.
 *	Copyright (C) 2012 Theophile BASTIAN
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "processingFunc.h"
using namespace std;

void checkErrors(const std::vector<Instruction>& instructions)
{
	int bracesCount = 0, ptrPos = 0, charNum = 0;

	for(std::vector<Instruction>::const_iterator instruct = instructions.begin(); instruct != instructions.end(); instruct++)
	{
		charNum += instruct->consecutive;

		if(instruct->type == LOOP_BEGIN)
			bracesCount+=instruct->consecutive;
		if(instruct->type == LOOP_END)
		{
			bracesCount-=instruct->consecutive;
			if(bracesCount < 0)
			{
				string error = "Sign n°";
				error+= intToStr(charNum+bracesCount+1) + ": No matching '[' for ']'.";
				throw runtime_error(error);
			}
		}
		else if(instruct->type == PTR_INCREMENT)
		{
			ptrPos += instruct->consecutive;
			if(ptrPos >= MEM_SIZE)
			{
				string error = "Sign n°";
				error+= intToStr(charNum - (MEM_SIZE + instruct->consecutive)) + ": Memory overflow.";
				throw runtime_error(error);
			}
		}
		else if(instruct->type == PTR_DECREMENT)
			ptrPos -= instruct->consecutive;
	}
	if(bracesCount > 0)
	{
		throw runtime_error("Control reached end of file without ']' matching '[' character.");
	}
}

string writeCCode(const std::vector<Instruction>& instructions)
{
	string code;
	for(std::vector<Instruction>::const_iterator instruct = instructions.begin(); instruct != instructions.end(); instruct++)
	{
		switch(instruct->type)
		{
			case MEM_INCREMENT:
				code+=string("*ptr+=")+intToStr(instruct->consecutive)+";\n";
				break;
			case MEM_DECREMENT:
				code+=string("*ptr-=")+intToStr(instruct->consecutive)+";\n";
				break;
			case PTR_INCREMENT:
				code+=string("ptr+=")+intToStr(instruct->consecutive)+";\n";
				break;
			case PTR_DECREMENT:
				code+=string("ptr-=")+intToStr(instruct->consecutive)+";\n";
				break;
			case PRINT_CHAR:
				for(int i=0; i<instruct->consecutive; i++)
					code+="putchar(*ptr);\n";
				break;
			case INPUT_CHAR:
				for(int i=0; i<instruct->consecutive; i++)
					code+="*ptr = getchar();\n";
				break;
			case LOOP_BEGIN:
				for(int i=0; i<instruct->consecutive; i++)
					code+="while(*ptr) {\n";
				break;
			case LOOP_END:
				for(int i=0; i<instruct->consecutive; i++)
					code+="}\n";
				break;
			default:
				break;
		}
	}
	return code;
}

std::string intToStr(int input)
{
	std::ostringstream oss;
	oss << input;
	return oss.str();
}

std::string parseCompileLine(const std::string& cCode, const std::string& output)
{
	string out=COMPILE;
	out.replace(out.find("$(code)"), 7, cCode);
	out.replace(out.find("$(output)"), 9, output);
	return out;
}

