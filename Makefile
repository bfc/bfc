CXX = g++
FLAGS = -W -Wall -O2
LIBS = 
TARGET = bfc
OBJ = main.o ioFunc.o processingFunc.o

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CXX) $(FLAGS) $(LIBS) $^ -o $@

%.o: %.cpp
	$(CXX) $(FLAGS) -c $< -o $@


clean:
	rm -f *.o

mrproper: clean
	rm -f $(TARGET)
