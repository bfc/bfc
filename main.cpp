
/*
 * PROGRAM:
 * 	BFC
 *
 * DESCRIPTION:
 * 	BFC - BrainFuck Compiler compiles a Brainfuck code to an executable file.
 * 
 * AUTHOR:
 * 	Theophile "Tobast" BASTIAN <contact@tobast.fr>
 *
 * LICENCE:
 *  BFC - See above for description.
 *	Copyright (C) 2012 Theophile BASTIAN
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include <iostream>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <cstdlib>

#include "ioFunc.h"
#include "processingFunc.h"
#include "data.h"

using namespace std;

int main(int argc, char** argv)
{
	if(argc < 3)
	{
		cerr << "FATAL: usage: bfc <source> <output>\n";
		return -1;
	}

	// Open source file
	ifstream source(argv[1]); 
	if(!source.is_open())
	{
		cerr << "FATAL: cannot open input file \"" << argv[1] << "\".\n";
		return -1;
	}
	
	// Tests the possiblity to create output file
	ofstream output(argv[2]);
	if(!output.is_open())
	{
		cerr << "FATAL: cannot open output file \"" << argv[2] << "\".\n";
		return -1;
	}
	output.close();

	
	// Fill an instruction vector
	vector<Instruction> instructions;
	fillInstructionVector(instructions, source);
	source.close();

	// Search for syntax errors
	try {
		checkErrors(instructions);
	}
	catch(const runtime_error& e) {
		cerr << "Error: " << e.what() << "\n";
		return -1;
	}

	// Turns BF code into C code
	string cCode = HEAD_CODE;
	cCode += writeCCode(instructions);
	cCode += FOOT_CODE;

	// Execute C compiler
	system(parseCompileLine(cCode, std::string(argv[2])).c_str());

	cout << "Done.\n";

	return 0;
}

