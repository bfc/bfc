
/*
 * PROGRAM:
 * 	BFC
 *
 * DESCRIPTION:
 * 	BFC - BrainFuck Compiler compiles a Brainfuck code to an executable file.
 * 
 * AUTHOR:
 * 	Theophile "Tobast" BASTIAN <contact@tobast.fr>
 *
 * LICENCE:
 *  BFC - See above for description.
 *	Copyright (C) 2012 Theophile BASTIAN
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#include "ioFunc.h"
using namespace std;

void fillInstructionVector(std::vector<Instruction>& instructions, std::ifstream& input)
{
	do {
		char curChar;
		input.get(curChar);

		InstructionType type = getInstructType(curChar);
		if(type==COMMENT)
			continue;

		if(!instructions.empty() && type == instructions.back().type)
			instructions.back().consecutive++;
		else
		{
			Instruction instruct(type);
			instructions.push_back(instruct);
		}
	} while(!input.eof());
}

InstructionType getInstructType(char input)
{
	switch(input)
	{
		case '+':
			return MEM_INCREMENT;
			break;
		case '-':
			return MEM_DECREMENT;
			break;
		case '>':
			return PTR_INCREMENT;
			break;
		case '<':
			return PTR_DECREMENT;
			break;
		case '.':
			return PRINT_CHAR;
			break;
		case ',':
			return INPUT_CHAR;
			break;
		case '[':
			return LOOP_BEGIN;
			break;
		case ']':
			return LOOP_END;
			break;
	}
	return COMMENT;
}

