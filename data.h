
/*
 * PROGRAM:
 * 	BFC
 *
 * DESCRIPTION:
 * 	BFC - BrainFuck Compiler compiles a Brainfuck code to an executable file.
 * 
 * AUTHOR:
 * 	Theophile "Tobast" BASTIAN <contact@tobast.fr>
 *
 * LICENCE:
 *  BFC - See above for description.
 *	Copyright (C) 2012 Theophile BASTIAN
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

#ifndef DEF_DATA
#define DEF_DATA

#include <string>

enum InstructionType
{
	COMMENT, MEM_INCREMENT, MEM_DECREMENT, PTR_INCREMENT, PTR_DECREMENT, PRINT_CHAR, INPUT_CHAR, LOOP_BEGIN, LOOP_END
};

struct Instruction
{
	Instruction(InstructionType inType, int inConsecutive=1) : type(inType), consecutive(inConsecutive)
	{}

	InstructionType type;
	int consecutive;
};

const int MEM_SIZE=30000;
const char MEM_SIZE_STR[]="30000";

const char COMPILE[] = "/bin/echo '$(code)' | /usr/bin/gcc -x c - -W -Wall -O2 -o $(output)";

const std::string HEAD_CODE = std::string("// Code created by BFC - BrainFuck Compiler by Theophile 'Tobast' BASTIAN\n#include <stdio.h>\nchar memory[")+std::string(MEM_SIZE_STR)+"];\nint main(void) {\n	char* ptr=memory;\n";
const std::string FOOT_CODE = "\n	return 0;\n}\n";

#endif// DEF_DATA
